# Base Net

Plantilla base para desarrollar aplicaciones en .NET, usando Entity Framework Code First, MVC 5, plantilla dashboard AdminLTE2

## Pasos iniciales

Tener en consideración los siguientes pasos para obtener y ejecuar el proyecto sin complicaciones

### Pre requisitos

Es necesario tener instalado 

```
Visual Studio 2015 o Visual Studio 2017
```

### Configurar IIS

El proyecto está configurado para ejecutarse en la siguiente url http://net/base, esto podría generar errores la primera vez que se carga.

* Crear una carpeta con nombre "NET" y dentro de ella otra con nombre "BASE", clonar dentro de BASE
* Abrir block de notas con permiso de administrador y editar el archivo C:\Windows\System32\drivers\etc\hosts, agregar la siguiente línea "127.0.0.1 net" y guardar


### Proyecto de inicio

Una vez que haya cargado la solución correctamente, seleccionar el proyecto BASE.WEB, clic derecho y Establecer como proyecto de inicio


### Actualizar los paquetes NuGets

Menú Herramientas / Administrador de paquetes NuGet / Administrar paquetes NuGet para la solución / clic en Restaurar

### Actualizar conexión a la BD

* En el proyecto BASE.DBCONTEXT, abrir el archivo App.config y editar la propiedad "Data Source" del connectionString
* En el proyecto BASE.WEB, abrir el archivo Web.config y editar la propiedad "Data Source" del connectionString

### Ejecutar la migración

* Menú Herramientas / Administrador de paquetes NuGet / Consola de Administrador de paquetes
* Seleccionar En el combo seleccionar "BASE.DBCONTEXT"
* Ejecutar el siguiente comando

```
Update-Database
```

* Verificar que en el servidor se haya creado la base de datos con el nombre "BaseDB"

### Agregar registros a la tabla Clientes

```
INSERT INTO [dbo].[Clientes]
		([FirstName],[LastName],[Deleted],[CreatedAt],[CreatedBy],[UpdatedAt],[UpdatedBy],[DeletedAt],[DeletedBy])
     VALUES
		('Gabriel', 'Diaz Bravo', 0, GETDATE(), 0,  GETDATE(), 0, NULL, NULL)
GO
```

### Permiso para acceder a la base de datos

* Abrir SSMS (Sql Server Management Studio)
* En la sección Security / Login, clic derecho "Agregar Nuevo Login"
* Nombre de login "IIS APPPOOL\NET"
* Seleccionar "Autenticación con Windows"
* Sección "User Mapping", Chequear la BD "BaseDB", seleccionar los roles "db_datareader", "db_datawriter" y "db_owner"