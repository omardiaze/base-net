﻿using System.Web;

namespace BASE.UTILS
{
    public class CurrentUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
    }

    public class CurrentUserHelper
    {
        public static CurrentUser Get
        {
            get
            {
                //var user = HttpContext.Current.User;

                //if (user == null)
                //{
                //    return null;
                //}
                //else if (string.IsNullOrEmpty(user.Identity.GetUserId()))
                //{
                //    return null;
                //}

                //return new CurrentUser
                //{
                //    UserId = user.Identity.GetUserId(),
                //    UserName = user.Identity.GetUserName(),
                //    Name = user.Identity.Name
                //};
                return new CurrentUser
                {
                    UserId = 1,
                    UserName = "odiaz",
                    Name = "Omar Diaz"
                };
            }
        }
    }
}
