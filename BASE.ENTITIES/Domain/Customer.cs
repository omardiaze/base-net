﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BASE.ENTITIES.Helper;

namespace BASE.ENTITIES.Domain
{
    public class Customer : AuditEntity, ISoftDeleted
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //eliminacion logica
        public bool? Deleted { get; set; }
    }
}
