﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using BASE.DBCONTEXTSCOPE;
using BASE.ENTITIES.Custom;
using BASE.ENTITIES.Domain;
using BASE.ENTITIES.Helper;
using BASE.REPOSITORY;
using BASE.UTILS;

namespace BASE.SERVICES
{
    public class CustomerService : ICustomerService
    {
        //private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly IRepository<Customer> _customerRepository;

        public CustomerService(
            IDbContextScopeFactory dbContextScopeFactory,
            IRepository<Customer> customerRepository
            )
        {
            _dbContextScopeFactory = dbContextScopeFactory;
            _customerRepository = customerRepository;
        }

        public IEnumerable<Customer> GetAll()
        {
            var result = new List<Customer>();

            try
            {
                using (var ctx = _dbContextScopeFactory.CreateReadOnly())
                {
                    result = _customerRepository.GetAll().ToList();
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                //logger.Error(e.Message);
            }

            return result;
        }

        public int GetTotal()
        {
            using (var ctx = _dbContextScopeFactory.CreateReadOnly())
            {

                int result = 0;

                if (typeof(Customer).GetInterfaces().Contains(typeof(ISoftDeleted)))
                {
                    result = _customerRepository.AsQueryable().Count(c => c.Deleted == false);
                }
                else
                {
                    result = _customerRepository.AsQueryable().Count();
                }

                return result;
            }
        }

        public int GetTotalFiltro(string search = null)
        {
            using (var ctx = _dbContextScopeFactory.CreateReadOnly())
            {

                int result = 0;

                if (typeof(Customer).GetInterfaces().Contains(typeof(ISoftDeleted)))
                {
                    //result = _customerRepository.AsQueryable().Count(c => c.Deleted == false && (search == null || c.FirstName == search || c.LastName == search));
                    result = _customerRepository.AsQueryable().Count(c => c.Deleted == false && (search == null || c.FirstName.Contains(search) || c.LastName.Contains(search)));
                }
                else
                {
                    result = _customerRepository.AsQueryable().Count(c=> search == null || c.FirstName.Contains(search) || c.LastName.Contains(search));
                }

                return result;
            }
        }

        public IEnumerable<Customer> GetAllPaging(int startRow, int showRows, string search = null, string orderColumn = null, string orderDirection  = null )
        {
            try
            {
                using (var ctx = _dbContextScopeFactory.CreateReadOnly())
                {
                    string order = orderColumn + " " + orderDirection;

                    var result = _customerRepository.AsQueryable().Select(c=>c)
                        //.Where(c => search == null || c.FirstName == search || c.LastName == search)
                        .Where(c => search == null || c.FirstName.Contains(search) || c.LastName.Contains(search))
                        .OrderBy(order)
                        .Skip(startRow).Take(showRows);

                    return result.ToList();
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                //logger.Error(e.Message);
            }
            return null;
            //return result;
        }

        public Customer Get(int id)
        {
            var result = new Customer();

            try
            {
                using (var ctx = _dbContextScopeFactory.Create())
                {
                    result = _customerRepository.SingleOrDefault(x => x.Id == id);
                }
            }
            catch (Exception e)
            {
                //logger.Error(e.Message);
            }

            return result;
        }

        public ResponseHelper InsertOrUpdate(Customer model)
        {
            var rh = new ResponseHelper();

            try
            {
                using (var ctx = _dbContextScopeFactory.Create())
                {
                    if (model.Id == 0)
                    {
                        _customerRepository.Insert(model);
                    }
                    else
                    {
                        _customerRepository.Update(model);
                    }

                    ctx.SaveChanges();
                    rh.SetResponse(true);
                }
            }
            catch (Exception e)
            {
                //logger.Error(e.Message);
            }

            return rh;
        }

        public int Delete(int id)
        {
            int response = 0;

            try
            {
                using (var ctx = _dbContextScopeFactory.Create())
                {
                    var model = _customerRepository.SingleOrDefault(x => x.Id == id);
                    _customerRepository.Delete(model);

                    response = ctx.SaveChanges();

                }
            }
            catch (Exception e)
            {
                //logger.Error(e.Message);
            }

            return response;
        }
    }
}
