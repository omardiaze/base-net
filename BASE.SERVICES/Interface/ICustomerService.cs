﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BASE.ENTITIES.Custom;
using BASE.ENTITIES.Domain;
using BASE.UTILS;

namespace BASE.SERVICES
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetAll();
        Customer Get(int id);
        ResponseHelper InsertOrUpdate(Customer model);
        int Delete(int id);
        IEnumerable<Customer> GetAllPaging(int startRow, int showRows, string search = null, string orderColumn = null, string orderDirection = null);
        int GetTotal();
        int GetTotalFiltro(string search = null);
    }
}
