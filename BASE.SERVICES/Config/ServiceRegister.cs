﻿using BASE.DBCONTEXTSCOPE;
using BASE.ENTITIES.Domain;
using BASE.REPOSITORY;
using LightInject;

namespace BASE.SERVICES.Config
{
    public class ServiceRegister : ICompositionRoot
    {
        public void Compose(IServiceRegistry container)
        {
            var ambientDbContextLocator = new AmbientDbContextLocator();

            container.Register<IDbContextScopeFactory>((x) => new DbContextScopeFactory(null));

            container.Register<IRepository<Customer>>((x) => new Repository<Customer>(ambientDbContextLocator));

            container.Register<ICustomerService, CustomerService>();
        }
    }
}
