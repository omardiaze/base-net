﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BASE.ENTITIES.Domain;

namespace BASE.DBCONTEXT.Mapping
{
    public class CustomerMapping : EntityTypeConfiguration<Customer>
    {
        public CustomerMapping()
        {
            ToTable("Clientes");
            HasKey(m => m.Id);
            Property(m => m.FirstName).IsRequired().HasMaxLength(100);
            Property(m => m.LastName).IsRequired().HasMaxLength(100);
        }
    }
}
