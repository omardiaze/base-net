﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BASE.ENTITIES.Domain;
using BASE.ENTITIES.Helper;
using BASE.UTILS;
using EntityFramework.DynamicFilters;

namespace BASE.DBCONTEXT
{
    public class BaseDbContext : DbContext
    {
        public BaseDbContext() : base("BaseConnection")
        {
        }

        public virtual DbSet<Customer> Customer { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            AddMyFilters(ref modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(BaseDbContext)));

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            MakeAudit();
            return base.SaveChanges();
        }

        public static BaseDbContext Create()
        {
            return new BaseDbContext();
        }

        private void MakeAudit()
        {
            var modifiedEntries = ChangeTracker.Entries().Where(
                x => x.Entity is AuditEntity
                    && (
                    x.State == EntityState.Added
                    || x.State == EntityState.Modified
                    || x.State == EntityState.Deleted
                )
            );

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as AuditEntity;
                if (entity != null)
                {
                    var date = DateTime.Now;
                    var userId = CurrentUserHelper.Get != null ? CurrentUserHelper.Get.UserId : 0;

                    if (entity is ISoftDeleted)
                    {
                        ((ISoftDeleted) entity).Deleted = false;
                    }

                    switch (entry.State)
                    {
                            case EntityState.Added:
                                entity.CreatedAt = date;
                                entity.CreatedBy = userId;
                                entity.UpdatedAt = date;
                                entity.UpdatedBy = userId;
                            break;

                            case EntityState.Modified:
                                base.Entry(entity).Property(x => x.CreatedAt).IsModified = false;
                                base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;

                                entity.UpdatedAt = date;
                                entity.UpdatedBy = userId;
                            break;

                            case EntityState.Deleted:
                                if (entry.State == EntityState.Deleted && entity is ISoftDeleted)
                                {
                                    entry.State = EntityState.Modified;
                                    entity.DeletedAt = date;
                                    entity.DeletedBy = userId;
                                    ((ISoftDeleted)entity).Deleted = true;
                                }
                            break;

                    }
                    
                }
            }
        }

        private void AddMyFilters(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Filter(Enums.MyFilters.IsDeleted.ToString(), (ISoftDeleted ea) => ea.Deleted, false);
        }
    }
}
