﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BASE.ENTITIES.Custom;
using BASE.ENTITIES.Domain;
using BASE.SERVICES;
using BASE.UTILS;

namespace BASE.WEB.Controllers
{
    public class CustomersController : Controller
    {

        private readonly ICustomerService _customerService = DependecyFactory.GetInstance<ICustomerService>();

        // GET: Customers
        public ActionResult Index()
        {
            //IEnumerable<Customer> model = _customerService.GetAll();
            //return View(model);
            return View();
        }

        // GET: Customers/GetPage
        [HttpPost]
        public JsonResult GetPage()
        {
            int _start = Convert.ToInt32(Request.Form["start"]);
            int _length = Convert.ToInt32(Request.Form["length"]);
            int _draw = Convert.ToInt32(Request.Form["draw"]);
            int _idOrderColummn = Convert.ToInt32(Request.Form["order[0][column]"]);

            string _orderColumn = null;
            string _orderDirection = null;
            string _search = null;

            if (!String.IsNullOrEmpty(Request.Form["search[value]"]))
                _search = Request.Form["search[value]"];

            if (!String.IsNullOrEmpty(Request.Form["columns["+ _idOrderColummn +"][data]"]))
                _orderColumn = Request.Form["columns[" + _idOrderColummn + "][data]"];

            if (!String.IsNullOrEmpty(Request.Form["order[0][dir]"]))
                _orderDirection = Request.Form["order[0][dir]"];

            int _total = _customerService.GetTotal();
            int _totalFiltro = _customerService.GetTotalFiltro(_search);

            IEnumerable<Customer> model = _customerService.GetAllPaging(_start, _length, _search, _orderColumn, _orderDirection);

            return Json(new
            {
                search = _search,
                draw = _draw,
                recordsTotal = _total,
                recordsFiltered = _totalFiltro,
                data = model,

            });
        }

        // GET: Customers/Details/5
        public ActionResult Details(int id)
        {
            var model = _customerService.Get(id);
            return View(model);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        [HttpPost]
        public ActionResult Create(Customer customer)
        {

            if (ModelState.IsValid)
            {
                _customerService.InsertOrUpdate(customer);
                return RedirectToAction("Index");
            }

            return View();

        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {
            var model = _customerService.Get(id);
            return View(model);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        public ActionResult Edit(Customer customer)
        {

            if (ModelState.IsValid)
            {
                var rh = _customerService.InsertOrUpdate(customer);
                if (rh.Response)
                {
                    return RedirectToAction("index");
                }
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost]
        public JsonResult Delete(int id)
        {
            int response = 0;

            response = _customerService.Delete(id);

            if (response > 0)
                return Json(new {value = 1, message = "Record deleted!"});
            else
                return Json(new {value = 0, message = "Can't delete record!"});

        }
    }
}
